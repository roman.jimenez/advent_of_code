use std::fs::File;
use std::io::{BufRead, BufReader};

fn main(){
    let file = File::open("./input.txt").unwrap();
    let reader = BufReader::new(file);
    let mut differences = vec![0;0];

    for line in reader.lines() {
        let line_result = line.unwrap();
        let diagnostic = line_result.chars().collect::<Vec<_>>();
        if differences.len() == 0 {
            differences = vec![0; diagnostic.len()];
        }
        for i in 0..diagnostic.len() {
            if diagnostic[i] == '0' {
                differences[i] -= 1;
            } else if diagnostic[i] == '1' {
                differences[i] += 1;
            }
        }
    }

    let mut gamma: Vec<char> = vec!['0';differences.len()];
    let mut epsilon: Vec<char> = vec!['0';differences.len()];
    for i in 0..differences.len() {
        if differences[i] < 0 {
            gamma[i] = '0';
            epsilon[i] = '1';
        } else if differences[i] > 0 {
            gamma[i] = '1';
            epsilon[i] = '0';
        }
    }

    // make the string
    let gamma_string = gamma.iter().collect::<String>();
    let epsilon_string = epsilon.iter().collect::<String>();
    // borrow the string to make the decimal
    let gamma_decimal = isize::from_str_radix(&gamma_string, 2).unwrap();
    let epsilon_decimal = isize::from_str_radix(&epsilon_string, 2).unwrap();
    println!("total power consumption: {:?}", gamma_decimal * epsilon_decimal);
}
