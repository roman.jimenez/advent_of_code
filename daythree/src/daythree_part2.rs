use std::fs::File;
use std::io::{BufRead, BufReader};

fn main(){
  let file = File::open("./input.txt").unwrap();
  let reader = BufReader::new(file);
  let mut all_diagnostics = Vec::new();

  for line in reader.lines() {
    let line_result = line.unwrap();
    let diagnostic = line_result.chars().collect::<Vec<_>>();
    all_diagnostics.push(diagnostic.to_vec());
  }
  let diagnostic_length = all_diagnostics[0].len();

  let mut oxygen: Vec<Vec<char>> = all_diagnostics.to_vec();
  let mut co2: Vec<Vec<char>> = all_diagnostics.to_vec();

  for i in 0..diagnostic_length {
    // determine oxygen
    let mut oxygen_zeros = 0;
    let mut oxygen_ones = 0;
    for oxy_idx in 0..oxygen.len() {
      if oxygen[oxy_idx][i] == '0' {
        oxygen_zeros += 1;
      } else {
        oxygen_ones += 1;
      }
    }
    // only reduce size if have size to reduce by
    if oxygen.len() > 1 {
      // more 0s then keep the 0s
      if oxygen_zeros > oxygen_ones {
        oxygen.retain(|oxygen_diagnostic| oxygen_diagnostic[i] == '0');
      // other wise if more 1s or equal amounts then keep 1
      } else {
        oxygen.retain(|oxygen_diagnostic| oxygen_diagnostic[i] == '1');
      }

    }

    // determine co2
    let mut co2_zeros = 0;
    let mut co2_ones = 0;
    for co2_idx in 0..co2.len() {
      if co2[co2_idx][i] == '0' {
        co2_zeros += 1;
      } else {
        co2_ones += 1;
      }
    }
    // only reduce size if have size to reduce by
    if co2.len() > 1 {
      if co2_zeros > co2_ones {
        co2.retain(|co2_diagnostic| co2_diagnostic[i] == '1');
      // other wise if more 1s or equal amounts then keep 0
      } else {
        co2.retain(|co2_diagnostic| co2_diagnostic[i] == '0');
      }

    }
  }

  let oxygen_string = oxygen[0].iter().collect::<String>();
  let co2_string = co2[0].iter().collect::<String>();
  let oxygen_decimal = isize::from_str_radix(&oxygen_string, 2).unwrap();
  let co2_decimal = isize::from_str_radix(&co2_string, 2).unwrap();
  println!("oxygen: {:?}", oxygen_decimal);
  println!("co2: {:?}", co2_decimal);
  println!("total life support rating: {:?}", oxygen_decimal * co2_decimal);
}
