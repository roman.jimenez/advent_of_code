use std::fs::File;
use std::io::{BufRead, BufReader};

fn main(){
    let file = File::open("./input.txt").unwrap();
    let reader = BufReader::new(file);
    let mut prev = None;
    let mut increase = 0;
    for line in reader.lines() {
        let current = Option::<u16>::from(line.unwrap().parse::<u16>().unwrap());
        if prev != None && prev < current {
            increase += 1;
        }
        prev = current;
    }
    println!("increases {}", increase);
}
