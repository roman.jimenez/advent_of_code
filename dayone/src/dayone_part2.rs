use std::fs::File;
use std::io::{BufRead, BufReader};

fn main(){
    let file = File::open("./input.txt").unwrap();
    let reader = BufReader::new(file);
    let values: Vec<_> = reader
        .lines()
        .map(|line_value| {
            line_value
                .unwrap()
                .parse::<u16>()
                .unwrap()
        })
    .collect();

    let mut prev = None;
    let mut increase = 0;
    for i in 2..values.len() {
        let sum = Some(values[i-2] + values[i-1] + values[i]);
        if prev != None && prev < sum {
            increase += 1;
        }
        prev = sum;
    }
    println!("increases {}", increase);
}
