use std::fs::File;
use std::cmp::{min, max};
use std::collections::HashMap;
use std::io::{BufRead, BufReader};

fn main(){
    let file = File::open("./input.txt").unwrap();
    let reader = BufReader::new(file);
    let mut vents = Vec::new();
    for line in reader.lines() {
        let unwrapped_line = line.unwrap();
        let from_to_pair = unwrapped_line
            .split("->")
            .map( |coordinate_pair|
                coordinate_pair
                .split(",")
                .map(|coordinates| coordinates
                    .trim()
                    .parse::<u16>()
                    .unwrap())
                .collect()
            )
            .collect::<Vec<Vec<u16>>>();
        vents.push(from_to_pair);
    }

    // create point map for paths
    let mut paths = HashMap::new();
    // get vertical and horizontal moves
    for vent in vents.iter() {
        let x1 = vent[0][0];
        let x2 = vent[1][0];
        let mut y1 = vent[0][1];
        let mut y2 = vent[1][1];
        if y1 == y2 {
            let start_x = min(x1, x2);
            let end_x = max(x1, x2);
            for x in start_x..=end_x {
                *paths.entry((x, y1)).or_insert(0) += 1;
            }
        } else if x1 == x2 {
            let start_y = min(y1, y2);
            let end_y = max(y1, y2);
            for y in start_y..=end_y {
                *paths.entry((x1, y)).or_insert(0) += 1;
            }
        } else {
            // going from x1 to x2
            if x1 < x2 {
                // if y1 is less than y2, then we are going up from y1 to y2
                if y1 < y2 {
                    for x in x1..=x2 {
                        *paths.entry((x, y1)).or_insert(0) += 1;
                        y1 += 1;
                    }
                // otherwise we are going down
                } else {
                    for x in x1..=x2 {
                        *paths.entry((x, y1)).or_insert(0) += 1;
                        y1 -= 1;
                    }
                }
            } else if x1 > x2 {
                // if y2 is less than y1 then we go up from y2 to y1
                if y2 < y1{
                    for x in x2..=x1 {
                        *paths.entry((x, y2)).or_insert(0) += 1;
                        y2 += 1;
                    }
                // otherwise we go down
                } else {
                    for x in x2..=x1 {
                        *paths.entry((x, y2)).or_insert(0) += 1;
                        y2 -= 1;
                    }
                }
            }
        }
    }

    let mut cross_paths = 0;
    for path in paths.keys() {
        let cross = paths.get(path).unwrap();
        if cross >= &2 {
            cross_paths += 1;
        }
    }

    println!("vents {:?}", vents.len());
    println!("crossovers {:?}", cross_paths);
}

