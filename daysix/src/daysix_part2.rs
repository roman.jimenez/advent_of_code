use std::fs::File;
use std::collections::HashMap;
use std::io::{BufRead, BufReader};

fn main(){
    let file = File::open("./input.txt").unwrap();
    let reader = BufReader::new(file);
    let mut fishes = Vec::new();
    for line in reader.lines() {
        let unwrapped_line: Vec<u128> = line
            .unwrap()
            .split(",")
            .map(|initial_fish|
                initial_fish
                .parse::<u128>()
                .unwrap())
            .collect();
        fishes = unwrapped_line;
    }

    let mut fish_map: HashMap<u128, u128> = HashMap::new();

    // build fish map
    for &fish in fishes.iter() {
        *fish_map.entry(fish).or_insert(0) += 1;
    }

    for _day in 0..256 {
        let fish_0 = *fish_map.get(&0).unwrap_or(&0);
        let fish_1 = *fish_map.get(&1).unwrap_or(&0);
        let fish_2 = *fish_map.get(&2).unwrap_or(&0);
        let fish_3 = *fish_map.get(&3).unwrap_or(&0);
        let fish_4 = *fish_map.get(&4).unwrap_or(&0);
        let fish_5 = *fish_map.get(&5).unwrap_or(&0);
        let fish_6 = *fish_map.get(&6).unwrap_or(&0);
        let fish_7 = *fish_map.get(&7).unwrap_or(&0);
        let fish_8 = *fish_map.get(&8).unwrap_or(&0);
        // move the fishes down a place (similar to -= 1)
        *fish_map.entry(0).or_insert(0) = fish_1;
        *fish_map.entry(1).or_insert(0) = fish_2;
        *fish_map.entry(2).or_insert(0) = fish_3;
        *fish_map.entry(3).or_insert(0) = fish_4;
        *fish_map.entry(4).or_insert(0) = fish_5;
        *fish_map.entry(5).or_insert(0) = fish_6;
        // 0 fish becomes 6, so does 7 -= 1
        *fish_map.entry(6).or_insert(0) = fish_7 + fish_0;
        *fish_map.entry(7).or_insert(0) = fish_8;
        // 0 fish creates 8, so all of those come here
        *fish_map.entry(8).or_insert(0) = fish_0;
    }

    let mut total_fish_count = 0;
    for fish in fish_map.keys() {
        total_fish_count += fish_map.get(fish).unwrap_or(&0);
    }
    println!("total fishes: {:}", total_fish_count);
}
