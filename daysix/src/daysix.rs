use std::fs::File;
use std::io::{BufRead, BufReader};

fn main(){
    let file = File::open("./input.txt").unwrap();
    let reader = BufReader::new(file);
    let mut fishes = Vec::new();
    for line in reader.lines() {
        let unwrapped_line: Vec<i16> = line
            .unwrap()
            .split(",")
            .map(|initial_fish|
                initial_fish
                .parse::<i16>()
                .unwrap())
            .collect();
        fishes = unwrapped_line;
    }

    for _day in 0..80 {
        let mut _fishes_to_add = 0;
        for fish_idx in 0..fishes.len() {
            let mut current_fish = fishes[fish_idx] - 1;
            if current_fish == -1 {
                current_fish = 6;
                _fishes_to_add += 1;
            }
            fishes[fish_idx] = current_fish
        }
        for _fishes_added in 0.._fishes_to_add {
            fishes.push(8);
        }
    }

    println!("fishes count: {:}", fishes.len());
}
