use std::fs::File;
use std::cmp::{max, min};
use std::io::{BufRead, BufReader};

fn main() { 
    let file = File::open("./input.txt").unwrap();
    let reader = BufReader::new(file);
    let mut crab_ships = Vec::new();
    for line in reader.lines() {
        let unwrapped_line: Vec<u128> = line
            .unwrap()
            .split(",")
            .map(|initial_crab_position|
                initial_crab_position
                .parse::<u128>()
                .unwrap())
            .collect();
        crab_ships = unwrapped_line;
    }
    let mut min_pos = u128::MAX;
    let mut max_pos = u128::MIN;
    for crab_ship in &crab_ships {
        min_pos = min(min_pos, *crab_ship);
        max_pos = max(max_pos, *crab_ship);
    }

    let mut min_fuel_used = u128::MAX;
    for current_pos in min_pos..=max_pos {
        let mut current_fuel_usage = 0;
        for crab_ship in &crab_ships {
            let distance_traveled = max(current_pos, *crab_ship) - min(current_pos, *crab_ship);
            // sum number range from 1 to n
            let updated_fuel_usage = (1..=distance_traveled).fold(0, |a, b| a + b);
            current_fuel_usage += updated_fuel_usage;
        }
        min_fuel_used = min(current_fuel_usage, min_fuel_used);
    }
    println!("{:?}", min_fuel_used);
}
