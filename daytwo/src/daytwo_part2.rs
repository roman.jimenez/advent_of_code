use std::fs::File;
use std::io::{BufRead, BufReader};

fn main(){
    let file = File::open("./input.txt").unwrap();
    let reader = BufReader::new(file);
    let mut depth = 0;
    let mut horizontal = 0;
    let mut aim = 0;

    for line in reader.lines() {
        let input_line = line.unwrap();
        let instruction = input_line.split(" ").collect::<Vec<&str>>();
        let direction = instruction[0];
        let amount = instruction[1].parse::<i32>().unwrap();
        match direction {
            "forward" => {
                horizontal += amount;
                depth += aim * amount;
            },
            "up" => aim -= amount,
            "down" => aim += amount,
            _ => println!("wtf lol")
        }
    }

    println!("depth {:#?}", depth);
    println!("horizontal {:#?}", horizontal);
    println!("total {:#?}", depth * horizontal);
}
