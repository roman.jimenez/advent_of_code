use std::fs::File;
use std::io::{BufRead, BufReader};

fn main(){
    let file = File::open("./input.txt").unwrap();
    let reader = BufReader::new(file);
    let mut boards = Vec::new();
    let mut move_input = Vec::new();
    let mut temp_building_board = Vec::new();
    for line in reader.lines() {
        let unwrapped: String = line.unwrap();
        // commas == move input
        if unwrapped.contains(','){
            let moves_line = unwrapped.split(",")
                .map(|input_move| String::from(input_move))
                .collect::<Vec<String>>();
            move_input = moves_line;
        } else {
            if unwrapped == "" {
                // push new board on new line (only if we have board)
                if temp_building_board.len() > 0 {
                    boards.push(temp_building_board);
                    temp_building_board = Vec::new();
                }
            } else {
                // make it vector of owned strings
                let mut board_row = unwrapped.split(" ")
                    .map(|value| String::from(value))
                    .collect::<Vec<String>>();
                // clean extra spacings...
                board_row.retain(|value| value != "");
                temp_building_board.push(board_row);
            }
        }
    }

    // remove the matching pieces from the moves
    let mut all_boards_win = false;
    let mut winning_number = 0;
    let mut winning_board: Vec<Vec<String>> = Vec::new();
    for move_count in 0..move_input.len() {
        let current_move = move_input[move_count].to_string();
        for b in 0..boards.len() {
            // borrow a mutable board
            // let current_board = &mut boards[b];
            for row in 0..boards[b].len() {
                for col in 0..boards[b][row].len() {
                    let current_board_pos = boards[b][row][col].to_string();
                    if current_board_pos == current_move {
                        boards[b][row][col] = "_".to_string();
                    }
                    if check_if_winning_board(&boards[b]) {
                        // copy winning number into string
                        all_boards_win = check_all_boards(&boards);
                        winning_number = String::from(&current_move)
                            .parse::<i32>()
                            .unwrap();
                        winning_board = boards[b].clone();
                    }
                    if all_boards_win {
                        break;
                    }
                }
                if all_boards_win {
                    break;
                }
            }
            if all_boards_win {
                break;
            }
        }
        if all_boards_win {
            break;
        }
    }

    // find sum
    let mut winning_sum = 0;
    for row in 0..winning_board.len() {
        for col in 0..winning_board[row].len() {
            if winning_board[row][col] != "_" {
                let parsed_value = winning_board[row][col].parse::<i32>().unwrap();
                winning_sum += parsed_value;
            }
        }
    }
    let winning_number_int = winning_number.to_string().parse::<i32>().unwrap();
    println!("winning board: {:?}", winning_board);
    println!("winning sum: {:?}", winning_sum);
    println!("winning number: {:?}", winning_number_int);
    println!("winning amount: {:?}", winning_sum * winning_number_int);
}

fn check_if_winning_board(board: &Vec<Vec<String>>) -> bool {
    // check all rows
    for row in 0..board.len() {
        let mut row_picked = 0;
        for col in 0..board[row].len() {
            if board[row][col] == "_" {
                row_picked += 1;
            }
        }
        if row_picked == board[row].len() {
            return true;
        }
    }

    for col in 0..board.len() {
        let mut column_picked = 0;
        for row in 0..board.len() {
            if board[row][col] == "_" {
                column_picked += 1;
            }
        }
        if column_picked == board.len() {
            return true;
        }
    }
    return false;
}

fn check_all_boards(boards: &Vec<Vec<Vec<String>>>) -> bool{
    for board in boards.iter() {
        if !check_if_winning_board(&board) {
            return false;
        }
    }
    return true;
}

